import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {NgBootstrapRoutingModule} from './ng-bootstrap-routing.module';
import {NgBootstrapMainComponent} from './ng-bootstrap-main/ng-bootstrap-main.component';
import {NgbAlertModule, NgbButtonsModule, NgbCollapseModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgBootstrapButtonsComponent} from './ng-bootstrap-buttons/ng-bootstrap-buttons.component';
import {FormsModule} from '@angular/forms';
import {NgBootstrapAlertComponent} from './ng-bootstrap-alert/ng-bootstrap-alert.component';
import {NgBootstrapCollapseComponent} from './ng-bootstrap-collapse/ng-bootstrap-collapse.component';
import {NgBootstrapDropdownComponent} from './ng-bootstrap-dropdown/ng-bootstrap-dropdown.component';
import {NgBootstrapModalComponent} from './ng-bootstrap-modal/ng-bootstrap-modal.component';
import {NgBootstrapTableComponent} from './ng-bootstrap-table/ng-bootstrap-table.component';
import {NgBootstrapCarouselComponent} from './ng-bootstrap-carousel/ng-bootstrap-carousel.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [NgBootstrapMainComponent, NgBootstrapButtonsComponent, NgBootstrapAlertComponent, NgBootstrapCollapseComponent, NgBootstrapDropdownComponent, NgBootstrapModalComponent, NgBootstrapTableComponent, NgBootstrapCarouselComponent],
  imports: [
    SharedModule,
    NgBootstrapRoutingModule,
    NgbModule
  ]
})
export class NgBootstrapModule {
}
