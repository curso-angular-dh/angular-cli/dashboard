import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgBootstrapButtonsComponent } from './ng-bootstrap-buttons.component';

describe('NgBootstrapButtonsComponent', () => {
  let component: NgBootstrapButtonsComponent;
  let fixture: ComponentFixture<NgBootstrapButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgBootstrapButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgBootstrapButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
