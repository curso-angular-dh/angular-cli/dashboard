import {Routes} from '@angular/router';
import {NgBootstrapMainComponent} from './ng-bootstrap-main/ng-bootstrap-main.component';
import {NgBootstrapButtonsComponent} from './ng-bootstrap-buttons/ng-bootstrap-buttons.component';
import {NgBootstrapAlertComponent} from './ng-bootstrap-alert/ng-bootstrap-alert.component';
import {NgBootstrapCollapseComponent} from './ng-bootstrap-collapse/ng-bootstrap-collapse.component';
import {NgBootstrapDropdownComponent} from './ng-bootstrap-dropdown/ng-bootstrap-dropdown.component';
import {NgBootstrapModalComponent} from './ng-bootstrap-modal/ng-bootstrap-modal.component';
import {NgBootstrapTableComponent} from './ng-bootstrap-table/ng-bootstrap-table.component';
import {NgBootstrapCarouselComponent} from './ng-bootstrap-carousel/ng-bootstrap-carousel.component';

export const NG_BOOTSTRAP_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: NgBootstrapMainComponent,
    children: [
      {
        path: 'alerts',
        component: NgBootstrapAlertComponent
      },
      {
        path: 'buttons',
        component: NgBootstrapButtonsComponent
      },
      {
        path: 'collapse',
        component: NgBootstrapCollapseComponent
      },
      {
        path: 'dropdown',
        component: NgBootstrapDropdownComponent
      },
      {
        path: 'modal',
        component: NgBootstrapModalComponent
      },
      {
        path: 'table',
        component: NgBootstrapTableComponent
      },
      {
        path: 'carousel',
        component: NgBootstrapCarouselComponent
      }
    ]
  }
];
