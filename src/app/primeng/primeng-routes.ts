import {Routes} from '@angular/router';
import {PrimengMainComponent} from './primeng-main/primeng-main.component';
import {PrimengDataComponent} from './primeng-data/primeng-data.component';
import {PrimengInputComponent} from './primeng-input/primeng-input.component';
import {PrimengButtonComponent} from './primeng-button/primeng-button.component';
import {PrimengPanelComponent} from './primeng-panel/primeng-panel.component';
import {PrimengMessagesComponent} from './primeng-messages/primeng-messages.component';
import {PrimengGalleriaComponent} from './primeng-galleria/primeng-galleria.component';

export const PRIMENG_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: PrimengMainComponent,
    children: [
      {
        path: 'data',
        component: PrimengDataComponent,
        data: {title: 'Laboratorio 3', content: 'This data was received from router and it is working!!!'},
      },
      {
        path: 'input',
        component: PrimengInputComponent
      },
      {
        path: 'button',
        component: PrimengButtonComponent
      },
      {
        path: 'panel',
        component: PrimengPanelComponent
      },
      {
        path: 'message',
        component: PrimengMessagesComponent
      },
      {
        path: 'galleria',
        component: PrimengGalleriaComponent
      },
      {
        path: '',
        redirectTo: '/primeng/data',
        pathMatch: 'full'
      }
    ]
  }
];
