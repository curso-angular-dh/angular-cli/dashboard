import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {PRIMENG_ROUTES_CONFIG} from './primeng-routes';


@NgModule({
  imports: [RouterModule.forChild(PRIMENG_ROUTES_CONFIG)],
  exports: [RouterModule]
})
export class PrimengRoutingModule {
}
