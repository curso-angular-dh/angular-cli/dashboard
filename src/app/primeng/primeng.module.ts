import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PrimengRoutingModule} from './primeng-routing.module';
import {PrimengMainComponent} from './primeng-main/primeng-main.component';
import {PrimengDataComponent} from './primeng-data/primeng-data.component';
import {PrimengInputComponent} from './primeng-input/primeng-input.component';
import {SliderModule} from 'primeng/slider';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {RatingModule} from 'primeng/rating';
import {AccordionModule, ButtonModule, CardModule, GalleriaModule, InputMaskModule} from 'primeng/primeng';
import {PrimengButtonComponent} from './primeng-button/primeng-button.component';
import {PrimengPanelComponent} from './primeng-panel/primeng-panel.component';
import {PrimengMessagesComponent} from './primeng-messages/primeng-messages.component';
import {ToastModule} from 'primeng/toast';
import {PrimengGalleriaComponent} from './primeng-galleria/primeng-galleria.component';

@NgModule({
  declarations: [PrimengMainComponent, PrimengDataComponent, PrimengInputComponent, PrimengButtonComponent, PrimengPanelComponent, PrimengMessagesComponent, PrimengGalleriaComponent],
  imports: [
    SharedModule,
    PrimengRoutingModule,
    SliderModule,
    RatingModule,
    InputMaskModule,
    ButtonModule,
    AccordionModule,
    CardModule,
    ToastModule,
    GalleriaModule
  ]
})
export class PrimengModule {
}
