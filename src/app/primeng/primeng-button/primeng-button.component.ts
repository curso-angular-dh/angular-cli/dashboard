import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-primeng-button',
  templateUrl: './primeng-button.component.html',
  styleUrls: ['./primeng-button.component.scss']
})
export class PrimengButtonComponent implements OnInit {

  public clicks: number;

  public isDisabled: boolean;

  constructor() {
    this.clicks = 0;
    this.isDisabled = false;
  }

  ngOnInit() {
  }

  public count(): void {
    this.clicks++;
  }

}
