import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimengDataComponent } from './primeng-data.component';

describe('PrimengDataComponent', () => {
  let component: PrimengDataComponent;
  let fixture: ComponentFixture<PrimengDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimengDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimengDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
