import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-primeng-galleria',
  templateUrl: './primeng-galleria.component.html',
  styleUrls: ['./primeng-galleria.component.scss']
})
export class PrimengGalleriaComponent implements OnInit {

  public images = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(
    (value, index) => {
      return {
        source: `https://picsum.photos/900/500?random&t=${Math.random()}`,
        alt: `Description for Image ${index + 1}`,
        title: `Title ${index + 1}`
      };
    });

  constructor() {
  }

  ngOnInit() {
  }

}
