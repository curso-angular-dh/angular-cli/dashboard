import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-primeng-panel',
  templateUrl: './primeng-panel.component.html',
  styleUrls: ['./primeng-panel.component.scss']
})
export class PrimengPanelComponent implements OnInit {

  public index: number;

  constructor() {
    this.index = -1;
  }

  ngOnInit() {
  }

  public openNext(): void {
    this.index = (this.index === 3) ? 0 : this.index + 1;
  }

  public openPrev(): void {
    this.index = (this.index <= 0) ? 3 : this.index - 1;
  }

}
