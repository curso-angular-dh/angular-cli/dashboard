import {Routes} from '@angular/router';
import {CustomMainComponent} from './custom-main/custom-main.component';
import {CustomFirstComponent} from './custom-first/custom-first.component';
import {CustomSecondComponent} from './custom-second/custom-second.component';
import {CustomDetailsComponent} from './custom-details/custom-details.component';
import {CustomGuard} from './custom.guard';
import {CustomCanDeactivateGuard} from './custom-can-deactivate.guard';

export const CUSTOM_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: CustomMainComponent,
    canActivate: [CustomGuard],
    data: {title: 'Another title!', second: 'asdfasfd'},
    children: [
      {
        path: 'first',
        component: CustomFirstComponent,
        canActivateChild: [CustomGuard],
        children: [
          {
            path: ':id',
            component: CustomDetailsComponent,
            canDeactivate: [CustomCanDeactivateGuard]
          }
        ]
      },
      {
        path: 'second',
        component: CustomSecondComponent
      }
    ]
  }
];
